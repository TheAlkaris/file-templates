## File Templates

File templates to populate the context menus for a quick and easy way to make files you need quick on the fly. Place these files inside your Templates folder.



- Desktop Entry
- HTML
- LUA
- Python 3
- Python
- Ruby
- Shell
- Text

These are just a bunch of stuff I commonly use from time to time when I need them. These will work for Linux specifically, can't say for MacOS or Windows, but try them if you will by placing them into the Templates directory.